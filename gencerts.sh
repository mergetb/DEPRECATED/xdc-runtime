#!/bin/bash

if [[ ! -f ca-key.pem ]] || [[ ! -f ca.pem ]]; then
  ./cfssl genkey -initca csr.json | ./cfssljson -bare ca
fi
