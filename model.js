
topo = {
  name: 'mprox',
  nodes: [ub('user'), ub('minikube')],
  switches: [cx('sw')],
  links: [
    Link('user', 1, 'sw', 1),
    Link('sw', 2, 'minikube', 1),
  ]
}

function ub(name) {
  return {
    name: name,
    image: 'ubuntu-1804',
    cpu: { cores: 4 },
    memory: { capacity: GB(8) },
    mounts: [
      {source: env.PWD, point: '/tmp/repo'}
    ]
  }
}

function cx(name) {
  return {
    name: name,
    proc: { cores: 2 },
    image: 'cumulusvx-3.5-mvrf',
  }
}
