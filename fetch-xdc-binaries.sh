#!/bin/bash

curl -OL https://build.mergetb.net/bin/xdc-api
chmod +x xdc-api

curl -OL https://build.mergetb.net/bin/xdc-gw
chmod +x xdc-gw

curl -OL https://build.mergetb.net/bin/xdc-proxy
chmod +x xdc-proxy

curl -OL https://build.mergetb.net/bin/jumpcd
chmod +x jumpcd

