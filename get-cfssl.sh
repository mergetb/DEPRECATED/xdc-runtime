#!/bin/bash

if [[ ! -f cfssl ]]; then
  curl -o cfssl -L https://pkg.cfssl.org/R1.2/cfssl_linux-amd64
  chmod +x cfssl
fi

if [[ ! -f cfssljson ]]; then
  curl -o cfssljson -L https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64
  chmod +x cfssljson
fi

