#!/bin/sh

set -x
set -e

export ETCDCTL_API=3

rm -f murphy*
ssh-keygen -t rsa -N "" -f ./murphy -C murphy@goodwu.net
pubkey=`cat murphy.pub`
fingerprint=`ssh-keygen -E md5 -lf murphy | awk '{ print $2 }' | sed 's/MD5://g'`

etcdctl put "/xdc/keys/murphy/$fingerprint" "$pubkey"

