# Merge XDC Runtime

This repo contains a mostly self contained example of the Merge experiment development container (XDC) runtime. This includes

- A Kubernetes deployment controller.
- An https proxy for Jupyter notebook environments.
- A jump container that provides external SSH access to containers.

## Up & Running

You'll need [Raven](https://gitlab.com/rygoo/raven#installing) and [Ansible](https://docs.ansible.com/ansible/2.7/installation_guide/intro_installation.html) installed.

**everything below is executed as root**

```
./get-cfssl.sh
./gencerts.sh
./install-roles.sh
rvn build
rvn deploy
rvn pingwait user minikube sw
rvn status
rvn configure
ansible-playbook -i .rvn/ansible-hosts network.yml
ansible-playbook -i .rvn/ansible-hosts k8s.yml
ansible-playbook -i .rvn/ansible-hosts etcd.yml
ansible-playbook -i .rvn/ansible-hosts docker.yml
./fetch-xdc-binaries.sh
```

`ssh` into minikube, launch `tmux` and spin up 6 shells

```
eval $(rvn ssh minikube)
sudo su
cd /tmp/repo
tmux
```

### Shell 0

```
./launch-xdc-manager.sh
```

### Shell 1

```
./launch-gateway.sh
```

### Shell 2

```
./launch-proxy.sh
```

### Shell 3
```
./jumpcd
```

### Shell 5
```
./run-jumpc.sh
```

This will start a jumpc container. You can ssh into it via `ssh -p 2202 -i
murphy merge@localhost`. You can also exec into it via `docker exec -it jumpc
/bin/sh`. As containers get spwaned, the `/etc/hosts` file in this container
will get updated accordingly.

### Shell 5

The first time you do this it will take forever and 9 years to complete the first spawn because the Jupyter container is a fatass. k8s will cache the container so subsequent runs will be snappier.

```
./spawn.sh
curl -k https://test.portal.blackmesa.mergetb.io:4433/login

./spawn2.sh
curl -k https://cake.cube.aperature.mergetb.io:4433/login

./destroy.sh
./destroy2.sh
```

The `:4433` port is an artifact of the current simplified environment where the proxy and controller API are running on the same host and both serve TLS on the same address. In production they will be on different addresses both using 443 so the explicit port in the URL will not be needed.

## Kubernetes Deployment Controller (KDC)

The Kubernetes deployment controller spawns and destroys XDCs in response to HTTPS requests. The API looks like this.

```protobuf
service Xdc {
  rpc Spawn(SpawnRequest) returns (SpawnResponse) {
    option (google.api.http) = {
      post: "/v0/spawn"
      body: "*"
    };
  }
  rpc Destroy(DestroyRequest) returns (DestroyResponse) {
    option (google.api.http) = {
      post: "/v0/destroy"
      body: "*"
    };
  }
  rpc Health(HealthRequest) returns (HealthResponse) {
    option (google.api.http) = {
      post: "/v0/health"
      body: "*"
    };
  }
}

message SpawnRequest {
  string token = 1;
  string project = 2;
  string experiment = 3;
  string name = 4;
}
message SpawnResponse {}

message DestroyRequest {
  string token = 1;
  string project = 2;
  string experiment = 3;
  string name = 4;
}
message DestroyResponse {}

message HealthRequest {}
message HealthResponse {
  string health = 1;
}
```

## XDC Proxy System (XPS)

The primary interface for XDCs is [Jupyter](https://jupyter.org). Jupyter notebooks are accessed via web interface over HTTPS. XDCs get DNS names according to the following scheme

```
<xdc-name>.<experiment-name>.<project-name>.mergetb.io
```

However, every name below `.mergetb.io` decays to the same IPv4 address (they have separate IPv6 addresses). The job of the XDC proxy system is to listen on this address and route HTTPS requests according to their server name indication (SNI) to the appropriate container address.

New proxy entries are added to the XPS by the KDC. When the KDC spins up a container, it waits for k8s to assign it an IP address. Once it has the IP address it sends a request to the XPS with the fully qualified domain name (FQDN) of the container with its target proxy address.

## Jump Container

The jump container is a docker container that has access to the k8s xdc container network and the internet. It acts as a jump off point from the internet at large into experiment development containers. The container has almost nothing on it and the file systems are strictly read only. It's only purpose in life is to be a jump point. Users can ssh through this container either explicitly or by using ssh proxyjump. There is one user on this container called `merge`. The public keys associated with `merge` are kept in the usual location ~/.ssh/authorized_keys. However, this file is a readonly mount from the host and is managed by a process called jumpcd which is described in a following section.

The `/etc/hosts` file is also a read only mount from the host that is managed by jumpcd. When new XDCs are spawned, `jumpcd` adds their hosts entries to this mounted file so users of the jumpc can access their XDCs by fqdn.


### jumpcd

jumpcd is a daemon that keeps a Merge jump container in sync with the current set of users and the currently running XDCs. jumpcd watches directories in the Merge etcd

- /jumpc/keys

When new users are added to the testbed, the workspace service generates and ssh key for them and puts the pubkey in this etcd directory. jumpcd monitors this directory for changes. If it does change, jumpcd generates a new authorized_keys chain for the jump container it is managing.  authorized key files are mounted from the host to the containers as read only mounts, so they can only be changed from outside the container.

- /jumpc/xdcs

When new XDCs are spun up, the xdc-manager creates entries in this etcd directory that map fqdns to ip addresses. jumpc monitors this directory, and when changed, it generates a new hosts at /var/jumpc/hosts which is read-only mounted into the jump container at /etc/hosts

![](jumpc.png)
