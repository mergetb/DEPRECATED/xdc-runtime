#!/bin/bash

docker run -it \
  --rm --name jumpc \
  --read-only \
  -v /var/jumpc/keychain:/home/merge/.ssh/authorized_keys:ro \
  -v /var/jumpc/hosts:/etc/hosts:ro \
  -p 2202:22 \
  quay.io/mergetb/jumpc
